#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

AP6212_WIFI_FW_SRC := vendor/bsp/rockchip/peripheral/wifi/ap6212_firmware
AP6212_WIFI_FW_DST = system/vendor/firmware

AP6212_WIFI_FW := fw_bcm43438a0_apsta.bin fw_bcm43438a0.bin fw_bcm43438a0_p2p.bin nvram_ap6212.txt

PRODUCT_COPY_FILES += \
  $(join $(patsubst %, $(AP6212_WIFI_FW_SRC)/%, $(AP6212_WIFI_FW)), $(patsubst %, :$(AP6212_WIFI_FW_DST)/%, $(AP6212_WIFI_FW)))

WIFI_DRIVER_HAL_MODULE := wifi_driver.$(soc_name)
WIFI_DRIVER_HAL_PERIPHERAL := ap6212
