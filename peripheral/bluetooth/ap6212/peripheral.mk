#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

AP6212_BT_SRC := hardware/bsp/rockchip/peripheral/bluetooth/ap6212
AP6212_BT_FW_SRC := vendor/bsp/rockchip/peripheral/bluetooth/ap6212_firmware
AP6212_BT_FW_DST := system/vendor/firmware

PRODUCT_COPY_FILES += \
    $(AP6212_BT_FW_SRC)/bcm43438a0.hcd:$(AP6212_BT_FW_DST)/bcm43438a0.hcd \
    $(AP6212_BT_SRC)/bt_vendor.conf:system/etc/bluetooth/bt_vendor.conf

# BCM bluetooth
BOARD_HAVE_BLUETOOTH_BCM := true
BOARD_CUSTOM_BT_CONFIG := $(AP6212_BT_SRC)/vnd_kylin.txt
