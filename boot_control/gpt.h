/*
 * Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HARDWARE_ROCKCHIP_BOOTCTL_HAL_GPT_H
#define HARDWARE_ROCKCHIP_BOOTCTL_HAL_GPT_H

#include <string>

#define GPT_HDR_SIGNATURE               0x5452415020494645ULL
#define MAX_GPT_ENTRIES                 128
#define MAX_SLOTS                       2

namespace rockchip_boot_control {

/* linux/include/efi.h */
typedef char16_t efi_char16_t;

typedef struct
{
    uint8_t b[16];
} efi_guid_t;

/* based on linux/fs/partitions/efi.h */
typedef struct _gpt_header {
    uint64_t signature;
    uint32_t revision;
    uint32_t header_size;
    uint32_t header_crc32;
    uint32_t reserved1;
    uint64_t my_lba;
    uint64_t alternate_lba;
    uint64_t first_usable_lba;
    uint64_t last_usable_lba;
    efi_guid_t disk_guid;
    uint64_t partition_entry_lba;
    uint32_t num_partition_entries;
    uint32_t sizeof_partition_entry;
    uint32_t partition_entry_array_crc32;
} __attribute__((packed)) gpt_header;

typedef union _slot_attributes {
    struct {
        uint64_t required_to_function:1;
        uint64_t no_block_io_protocol:1;
        uint64_t legacy_bios_bootable:1;
        uint64_t priority:4;
        uint64_t tries:4;
        uint64_t successful:1;
        uint64_t reserved:36;
        uint64_t type_guid_specific:16;
    } fields;
    uint64_t raw;
} __attribute__((packed)) slot_attributes;

#define PARTNAME_SZ     (72 / sizeof(efi_char16_t))
typedef struct _gpt_entry {
    efi_guid_t partition_type_guid;
    efi_guid_t unique_partition_guid;
    uint64_t starting_lba;
    uint64_t ending_lba;
    slot_attributes attributes;
    efi_char16_t partition_name[PARTNAME_SZ];
} __attribute__((packed)) gpt_entry;

class PartitionTables {
public:
   /*
    * read_partitions() reads the header and partition array from
    * primary or secondary GPT of the disk. Secondary GPT is read in case
    * primary GPT fails sanity check, errni is set if both GPT copies
    * fail sanity check.
    * Returns pointer to allocated object on success,
    * and 'nullptr' on error with errno set.
    * On failure this method will log to stderr.
    */
    static std::unique_ptr<PartitionTables>
                           read_partitions(const std::string& device);

   /*
    * write_partitions() writes the header and partition array to
    * primary and secondary GPT on the disk.
    * Returns 0 on success, -errno on error.
    */
    int write_partitions();

    /*
     * getIndexForSlottedBootPartition() gets the partition index associated
     * with the slot parameter passed.
     * Returns 0 on success, and partition_index value is valid.
     * REtrns -errno on error, the partition_index value is invalid.
     */
    int getIndexForSlottedBootPartition(unsigned slot,
                                        uint32_t& partition_index) const;

    /*
     * gpt_sanity_check() checks the header for correctness of signature,
     * size and CRC.
     * Returns 'true' on success, 'false' on error.
     */
    bool gpt_sanity_check();

    gpt_header header;
    gpt_entry partition_array[MAX_GPT_ENTRIES];

private:
    std::string disk_device = "";
};

}; //rockchip_boot_control

#endif
